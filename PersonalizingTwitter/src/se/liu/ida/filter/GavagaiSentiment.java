package se.liu.ida.filter;

public class GavagaiSentiment implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	double skepticism;
	double fear;
	double positivity;
	double negativity;
	double love;
	double desire;
	double hate;
	double violence;

	
	public GavagaiSentiment(double s, double f, double p, double n, double l, double d, double h, double v)
	{
		this.skepticism = s;
		this.fear = f;
		this.positivity = p;
		this.negativity = n;
		this.love = l;
		this.desire = d;
		this.hate = h;
		this.violence = v;
	}


	public GavagaiSentiment(double[] values) {
		this.skepticism = values[0];
		this.fear = values[1];
		this.positivity = values[2];
		this.negativity = values[3];
		this.love = values[4];
		this.desire = values[5];
		this.hate = values[6];
		this.violence = values[7];
	}


	public double getSkepticism() {
		return skepticism;
	}


	public double getFear() {
		return fear;
	}


	public double getPositivity() {
		return positivity;
	}


	public double getNegativity() {
		return negativity;
	}


	public double getLove() {
		return love;
	}


	public double getDesire() {
		return desire;
	}


	public double getHate() {
		return hate;
	}


	public double getViolence() {
		return violence;
	}
	
	
	public String toString()
	{
		return "skepticism: " + this.skepticism  + "\n" + 
				"fear: " + this.fear + "\n" +
				"positivity: " + this.positivity  + "\n" + 
				"negativity: " + this.negativity  + "\n" + 
				"love: " + this.love + "\n" + 
				"desire: " + this.desire + "\n" + 
				"hate: " + this.hate + "\n" + 
				"violence: " + this.violence;
		
	}


	public boolean isNeutralSentiment() {
		if (this.skepticism == 0 &&
				this.fear == 0 &&
				this.positivity == 0 &&
				this.negativity == 0 &&
				this.love == 0 &&
				this.desire == 0 &&
				this.hate == 0 &&
				this.violence == 0)
		{
			return true;
		}
		return false;
	}
	
	/** Calculates the hamming distance between two sentiments */
	public double getHammingDistance(GavagaiSentiment sentiment2){
		double sum =0;
		sum +=Math.abs(this.desire-sentiment2.desire);
		sum +=Math.abs(this.fear-sentiment2.fear);
		sum +=Math.abs(this.hate-sentiment2.hate);
		sum +=Math.abs(this.love-sentiment2.love);
		sum +=Math.abs(this.negativity-sentiment2.negativity);
		sum +=Math.abs(this.positivity-sentiment2.positivity);
		sum +=Math.abs(this.skepticism-sentiment2.skepticism);
		sum +=Math.abs(this.violence-sentiment2.violence);
		return sum;
	}


}
